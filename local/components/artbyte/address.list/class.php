<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Test\HL;
use Bitrix\Main\Grid\Options as GridOptions;
use Bitrix\Main\UI\PageNavigation;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @var $APPLICATION CMain
 * @var $USER CUser
 */

Loc::loadMessages(__FILE__);


class AddressList extends \CBitrixComponent
{

    private const MODULES = [
        'highloadblock'
    ];
    private const GRID_ID = 'user_address_list';
    private $filter = [];
    private $select = [];
    private $gridOptions;
    private $gridSort;
    private $nav;

    private function loadModules()
    {
        foreach (self::MODULES as $module) {
            if (!Loader::includeModule($module)) {
                ShowError(Loc::getMessage('INCLUDE_MODULE_ERROR', ['#MODULE#' => $module]));
                return false;
            }
        }
        return true;
    }

    private function initGridOptions()
    {
        $this->gridOptions = new GridOptions($this::GRID_ID);
    }

    private function initGridSort()
    {
        $this->gridSort = $this->gridOptions->GetSorting();
    }

    private function initNav()
    {
        $navParams = $this->gridOptions->GetNavParams();
        $nav = new PageNavigation($this::GRID_ID);
        $nav->allowAllRecords(true)
            ->setPageSize($navParams['nPageSize'])
            ->initFromUri();
        $this->arResult['NAV'] = $this->nav = $nav;
    }

    private function initGridId()
    {
        $this->arResult['GRID_ID'] = $this::GRID_ID;
    }

    private function initGridColumns()
    {
        $this->arResult['COLUMNS'] = [
            [
                'id' => 'ID',
                'name' => 'ID',
                'sort' => 'ID',
                'default' => false
            ],
            [
                'id' => HL::ADDRESS_FIELD,
                'name' => Loc::getMessage('GRID_ADDRESS'),
                'sort' => HL::ADDRESS_FIELD,
                'default' => true
            ],
            [
                'id' => HL::ACTIVE_FIELD,
                'name' => Loc::getMessage('GRID_ACTIVE'),
                'sort' => HL::ACTIVE_FIELD,
                'default' => true
            ],
        ];
    }

    private function initGridPageSizes()
    {
        $this->arResult['PAGE_SIZES'] = [
            ['NAME' => "1", 'VALUE' => '1'],
            ['NAME' => "5", 'VALUE' => '5'],
            ['NAME' => '10', 'VALUE' => '10'],
            ['NAME' => '20', 'VALUE' => '20'],
        ];
    }

    private function prepareGridOptions()
    {
        $this->initGridOptions();
        $this->initGridSort();
        $this->initNav();
        $this->initGridId();
        $this->initGridColumns();
        $this->initGridPageSizes();
    }

    private function prepareFilter()
    {
        global $USER;

        $this->filter[HL::USER_ID_FIELD] = $USER->GetId();
        if ($this->arParams['ONLY_ACTIVE'] === 'Y') {
            $this->filter[HL::ACTIVE_FIELD] = 1;
        }
    }

    private function prepareSelect()
    {
        $this->select = ['*'];
    }

    private function prepareResult()
    {
        $rows = $this->getList();
        $data = [];
        foreach ($rows as $row) {
            $data[$row['ID']] = [
                'data'    => [
                    'ID' => $row['ID'],
                    HL::ADDRESS_FIELD => $row[HL::ADDRESS_FIELD],
                    HL::ACTIVE_FIELD => $row[HL::ACTIVE_FIELD] ? Loc::getMessage('ACTIVE_Y')
                        : Loc::getMessage('ACTIVE_N'),
                ],
                'actions' => [
                ]
            ];
        }
        $this->arResult['ROWS'] = $data;
    }

    public function onPrepareComponentParams($arParams)
    {
        if (!isset($arParams['ONLY_ACTIVE'])) {
            $arParams['ONLY_ACTIVE'] = 'Y';
        }
        if(!isset($arParams["CACHE_TIME"])) {
            $arParams["CACHE_TIME"] = 36000000;
        }
        return $arParams;
    }

    public function executeComponent()
    {
        if (!$this->loadModules()) {
            return;
        }
        $this->prepareGridOptions();
        $this->prepareSelect();
        $this->prepareFilter();
        $this->prepareResult();
        $this->includeComponentTemplate();
    }

    public function getList()
    {
        $dataClass = HL::getDataClass();
        $result = $dataClass::getList([
            'select' => $this->select,
            'filter' => $this->filter,
            'order' => $this->gridSort['sort'],
            'offset' => $this->nav->getOffset(),
            'limit' => $this->nav->getLimit(),
            'count_total' => true,
            'cache' => ['ttl' => $this->arParams['CACHE_TIME']]
        ]);
        $this->arResult['NAV']->setRecordCount($result->getCount());
        return $result->fetchAll();

    }

}