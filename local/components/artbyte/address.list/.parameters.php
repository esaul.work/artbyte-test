<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentParameters = [
    "GROUPS" => [
        "MAIN_SETTINGS" => [
            "NAME" => GetMessage("BUS_MAIN_SETTINGS")
        ]
    ],
];

$arComponentParameters["PARAMETERS"]['ONLY_ACTIVE'] = [
    "NAME" => GetMessage("BUS_ONLY_ACTIVE"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
    "PARENT" => "MAIN_SETTINGS"
];
