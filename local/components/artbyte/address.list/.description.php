<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

$arComponentDescription = array(
	"NAME" => GetMessage("BUS_NAME"),
	"DESCRIPTION" => GetMessage("BUS_DESCR"),
	"PATH" => array(
		"ID" => "bus_test",
		"CHILD" => array(
			"ID" => "address_list",
			"NAME" => GetMessage("BUS_COMP_NAME")
		)
	),
);
