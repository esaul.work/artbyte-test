<?php
$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('', 'TestBusArtbyteOnAfterAdd', 'clearTestBusArtbyteCache');
$eventManager->addEventHandler('', 'TestBusArtbyteOnAfterUpdate', 'clearTestBusArtbyteCache');
$eventManager->addEventHandler('', 'TestBusArtbyteOnAfterDelete', 'clearTestBusArtbyteCache');

function clearTestBusArtbyteCache($event)
{
    $event->getEntity()->cleanCache();
}