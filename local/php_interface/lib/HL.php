<?php

namespace Test;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;

\CModule::IncludeModule('highloadblock');

class HL
{

    private const ENTITY_PREFIX = 'HLBLOCK_';

    private static $HLName = 'TestBusArtbyte';

    public const ACTIVE_FIELD = 'UF_ACTIVE';
    public const USER_ID_FIELD = 'UF_USER_ID';
    public const ADDRESS_FIELD = 'UF_ADDRESS';

    /**
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getHL()
    {
        return HighloadBlockTable::getList([
            'filter' => ['NAME' => self::$HLName]
        ])->fetch();
    }

    /**
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getEntityIdHL()
    {
        return self::ENTITY_PREFIX . self::getHL()['ID'];
    }

    /**
     * @return \Bitrix\Main\Entity\Base
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getEntity()
    {
        return HighloadBlockTable::compileEntity(self::getHL());
    }

    /**
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getDataClass()
    {
        return self::getEntity()->getDataClass();
    }

    /**
     * @return string
     */
    public function getHLName(): string
    {
        return $this->HLName;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }
}